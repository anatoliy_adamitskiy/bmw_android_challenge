package com.bmw_android_challenge.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.bmw_android_challenge.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) protected Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( getLayoutResId() );
        ButterKnife.bind(this);

        initData();
        initViews();
        initToolbar();
    }

    protected void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    protected int getFragmentContainerId() {
        return R.id.fragment_container;
    }

    protected void initData(){

    }

    abstract void initViews();
    abstract int getLayoutResId();
}
