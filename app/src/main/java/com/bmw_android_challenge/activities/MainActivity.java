package com.bmw_android_challenge.activities;

import com.bmw_android_challenge.R;
import com.bmw_android_challenge.fragments.MainFragment;

public class MainActivity extends BaseActivity {

    @Override
    void initViews() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                MainFragment.getInstance()).commit();
    }

    @Override
    int getLayoutResId() {
        return R.layout.activity_base;
    }

}
