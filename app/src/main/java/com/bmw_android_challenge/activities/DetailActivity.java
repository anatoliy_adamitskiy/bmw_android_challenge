package com.bmw_android_challenge.activities;

import android.view.MenuItem;

import com.bmw_android_challenge.R;
import com.bmw_android_challenge.fragments.DetailFragment;
import com.bmw_android_challenge.models.Location;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

public class DetailActivity extends BaseActivity implements OnMapReadyCallback {

    private MapFragment mMapFragment;
    private Location mLocation;

    @Override
    void initViews() {
        mMapFragment = MapFragment.newInstance();
        mMapFragment.getMapAsync(this);
        mLocation = Parcels.unwrap(getIntent().getParcelableExtra(getString(R.string.extra_location)));
        initFragments();
    }

    @Override
    int getLayoutResId() {
        return R.layout.activity_detail;
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mLocation.getName());
        }
    }

    // Initializing all needed fragments
    public void initFragments() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_map,
                mMapFragment).commit();
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_details,
                DetailFragment.getInstance()).commit();
    }

    // Map callback when done loading
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mLocation != null && mMapFragment != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 16));
            googleMap.addMarker(new MarkerOptions().anchor(0.0f, 1.0f)
                    .position(new LatLng(mLocation.getLatitude(), mLocation.getLongitude())));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
