package com.bmw_android_challenge.database;

import android.provider.BaseColumns;

public class LocationDbContract {

    public static String[] projection = {
            LocationEntry._ID,
            LocationEntry.COLUMN_NAME_ENTRY_ID,
            LocationEntry.COLUMN_NAME_NAME,
            LocationEntry.COLUMN_NAME_LATITUDE,
            LocationEntry.COLUMN_NAME_LONGITUDE,
            LocationEntry.COLUMN_NAME_ADDRESS,
            LocationEntry.COLUMN_NAME_ARRIVAL_TIME
    };

    public LocationDbContract() {}

    public static abstract class LocationEntry implements BaseColumns {
        public static final String TABLE_NAME = "locations";
        public static final String COLUMN_NAME_ENTRY_ID = "id";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_ARRIVAL_TIME = "arrivalTime";
    }

}
