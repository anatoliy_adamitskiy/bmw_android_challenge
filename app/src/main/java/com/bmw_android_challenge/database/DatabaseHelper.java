package com.bmw_android_challenge.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bmw_android_challenge.models.Location;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Database.sqlite";
    private static final int DATABASE_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private static DatabaseHelper mDbHelper;

    // Delete Tables
    private static final String SQL_DELETE_LOCATION_TABLE = "DROP TABLE IF EXISTS " + LocationDbContract.LocationEntry.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super( context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    public static void initialize(Context context) {
        if( mDbHelper == null ) {
            mDbHelper = new DatabaseHelper(context);
        }
    }

    public static DatabaseHelper getInstance() {
        return mDbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL( SQL_CREATE_LOCATION_TABLE );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL( SQL_DELETE_LOCATION_TABLE );
        onCreate( db );
    }

    // Create Tables
    private static final String SQL_CREATE_LOCATION_TABLE =
            "CREATE TABLE " + LocationDbContract.LocationEntry.TABLE_NAME + " (" +
                    LocationDbContract.LocationEntry._ID + " INTEGER PRIMARY KEY," +
                    LocationDbContract.LocationEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
                    LocationDbContract.LocationEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    LocationDbContract.LocationEntry.COLUMN_NAME_LATITUDE + TEXT_TYPE + COMMA_SEP +
                    LocationDbContract.LocationEntry.COLUMN_NAME_LONGITUDE + TEXT_TYPE + COMMA_SEP +
                    LocationDbContract.LocationEntry.COLUMN_NAME_ADDRESS + TEXT_TYPE + COMMA_SEP +
                    LocationDbContract.LocationEntry.COLUMN_NAME_ARRIVAL_TIME + TEXT_TYPE +
                    " )";

    // Inserting Objects
    public void insertLocation(Location location) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = getReadableDatabase();

        values.put( LocationDbContract.LocationEntry.COLUMN_NAME_ENTRY_ID, location.getId());
        values.put( LocationDbContract.LocationEntry.COLUMN_NAME_NAME, location.getName());
        values.put( LocationDbContract.LocationEntry.COLUMN_NAME_LATITUDE, location.getLatitude());
        values.put( LocationDbContract.LocationEntry.COLUMN_NAME_LONGITUDE, location.getLongitude());
        values.put( LocationDbContract.LocationEntry.COLUMN_NAME_ADDRESS, location.getAddress());
        values.put( LocationDbContract.LocationEntry.COLUMN_NAME_ARRIVAL_TIME, location.getArrivalTime());

        db.insert(
                LocationDbContract.LocationEntry.TABLE_NAME,
                null,
                values );
    }

    // Data Retrieval and Manipulation
    public List<Location> getAllLocationsFromReadableDatabase() {
        SQLiteDatabase db = getReadableDatabase();
        String sortOrder =
                LocationDbContract.LocationEntry.COLUMN_NAME_ENTRY_ID + " ASC";
        Cursor c = db.query(
                LocationDbContract.LocationEntry.TABLE_NAME,  // The table to query
                LocationDbContract.projection,                  // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        List<Location> locations = new ArrayList<>(c.getCount());

        for( c.moveToFirst(); !c.isAfterLast(); c.moveToNext() ) {
            locations.add( new Location(
                    Integer.parseInt(c.getString(c.getColumnIndex(LocationDbContract.LocationEntry.COLUMN_NAME_ENTRY_ID))),
                    c.getString(c.getColumnIndex(LocationDbContract.LocationEntry.COLUMN_NAME_NAME)),
                    Double.parseDouble(c.getString(c.getColumnIndex(LocationDbContract.LocationEntry.COLUMN_NAME_LATITUDE))),
                    Double.parseDouble(c.getString(c.getColumnIndex(LocationDbContract.LocationEntry.COLUMN_NAME_LONGITUDE))),
                    c.getString(c.getColumnIndex(LocationDbContract.LocationEntry.COLUMN_NAME_ADDRESS)),
                    c.getString(c.getColumnIndex(LocationDbContract.LocationEntry.COLUMN_NAME_ARRIVAL_TIME))
            ));

        }

        c.close();
        return locations;
    }

}
