package com.bmw_android_challenge.fragments;

import android.app.ProgressDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bmw_android_challenge.R;
import com.bmw_android_challenge.adapters.LocationAdapter;
import com.bmw_android_challenge.database.DatabaseHelper;
import com.bmw_android_challenge.models.Location;
import com.bmw_android_challenge.utils.DatabaseUtils;
import com.bmw_android_challenge.utils.LocationRequest;
import com.bmw_android_challenge.utils.LocationResultCallback;

import java.util.List;

import butterknife.BindView;

public class MainFragment extends BaseFragment implements LocationResultCallback {

    @BindView(R.id.recycler_view) protected RecyclerView mRecyclerView;

    private LocationAdapter mAdapter;
    private ProgressDialog mDialog;

    public static MainFragment getInstance() {
        MainFragment fragment = new MainFragment();
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    void initViews() {
        initProgressDialog();
        requestData();
    }

    @Override
    int getLayoutResId() {
        return R.layout.fragment_recycler_view;
    }

    @Override
    int getMenuResId() {
        return R.menu.menu_main;
    }

    private void requestData() {
        if (!DatabaseUtils.doesDatabaseExist(getActivity(), DatabaseHelper.DATABASE_NAME)) {
            executeRequest(getString(R.string.api_url_locations));
        } else {
            setLocationsToAdapter(DatabaseHelper.getInstance()
                    .getAllLocationsFromReadableDatabase());
        }
    }

    // Execution of async request
    private void executeRequest(String url) {
        showProgressDialog();

        LocationRequest request = new LocationRequest();
        request.callback = this;
        request.execute(url);
    }

    @Override
    public void setResultsToAdapter(List<Location> results) {
        dismissProgressDialog();
        setLocationsToAdapter(results);
    }

    // Setting items to adapter
    private void setLocationsToAdapter(List<Location> locations) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        mAdapter = new LocationAdapter(getActivity());
        mAdapter.setItems(locations);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    // Progress Dialog Actions
    private void initProgressDialog() {
        mDialog = new ProgressDialog(getActivity());
    }

    private void showProgressDialog() {
        if (mDialog != null) {
            mDialog.setMessage("Loading Data...");
            mDialog.show();
        }
    }

    private void dismissProgressDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}
