package com.bmw_android_challenge.fragments;

import android.widget.TextView;

import com.bmw_android_challenge.R;
import com.bmw_android_challenge.models.Location;
import com.bmw_android_challenge.utils.DateTimeUtils;

import org.ocpsoft.prettytime.PrettyTime;
import org.parceler.Parcels;

import java.util.Date;

import butterknife.BindView;

public class DetailFragment extends BaseFragment {

    @BindView(R.id.arrival_time) protected TextView mArrivalTime;
    @BindView(R.id.location) protected TextView mLocationName;
    @BindView(R.id.address) protected TextView mAddress;
    @BindView(R.id.latitude) protected TextView mLatitude;
    @BindView(R.id.longitude) protected TextView mLongitude;

    private Location mLocation;

    public static DetailFragment getInstance() {
        DetailFragment fragment = new DetailFragment();
        return fragment;
    }

    @Override
    void initViews() {
        mLocation = Parcels.unwrap(getActivity().getIntent().getParcelableExtra(getString(R.string.extra_location)));
        setViews();
    }

    @Override
    int getLayoutResId() {
        return R.layout.fragment_detail;
    }

    @Override
    int getMenuResId() {
        return R.menu.menu_detail;
    }

    // Setting all text views
    public void setViews() {
        if (mLocation != null) {
            PrettyTime time = new PrettyTime();
            Date date = DateTimeUtils.getDateFromString(mLocation.getArrivalTime());
            if (mLocation.getArrivalTime() != null) {
                mArrivalTime.setText(String.format(getString(R.string.arrival_time), time.format(date)));
            }
            if (mLocation.getName() != null) {
                mLocationName.setText(mLocation.getName());
            }
            if (mLocation.getAddress() != null) {
                mAddress.setText(mLocation.getAddress());
            }
            if (mLocation.getLatitude() != 0) {
                mLatitude.setText(String.format(getString(R.string.latitude),
                        String.format("%1.2f", mLocation.getLatitude())));
            }
            if (mLocation.getLongitude() != 0) {
                mLongitude.setText(String.format(getString(R.string.longitude),
                        String.format("%1.2f", mLocation.getLongitude())));
            }
        }
    }

}
