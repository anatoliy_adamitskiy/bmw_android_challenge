package com.bmw_android_challenge.utils;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.bmw_android_challenge.database.DatabaseHelper;
import com.bmw_android_challenge.models.Location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class LocationRequest extends AsyncTask<String, Void, List<Location>> {

    public LocationResultCallback callback = null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected List<Location> doInBackground(String... params) {

        List<Location> locationResults = new ArrayList<>();
        JSONArray locationArray = null;

        SQLiteDatabase db = DatabaseHelper.getInstance().getWritableDatabase();
        DatabaseHelper.getInstance().onUpgrade(db, 1, 1);

        try {
            URL url = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.connect();
            InputStream is = connection.getInputStream();
            String data = convertStreamToString(is);
            is.close();
            locationArray = new JSONArray(data);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        if (locationArray != null) {
            for (int i = 0; i < locationArray.length(); i++) {
                JSONObject object = null;
                Location location = null;
                int mId = 0;
                String mName = null;
                double mLat = 0;
                double mLong = 0;
                String mAddress = null;
                String mArrivalTime = null;

                try {
                    object = (JSONObject) locationArray.get(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mId = object.getInt("ID");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mName = object.getString("Name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mLat = object.getDouble("Latitude");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mLong = object.getDouble("Longitude");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mAddress = object.getString("Address");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mArrivalTime = object.getString("ArrivalTime");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                location = new Location(mId, mName, mLat, mLong, mAddress, mArrivalTime);

                if (location != null) {
                    locationResults.add(location);
                    DatabaseHelper.getInstance().insertLocation(location);
                }
            }
        }

        return locationResults;
    }

    @Override
    protected void onPostExecute(List<Location> results) {
        callback.setResultsToAdapter(results);
    }

    static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}