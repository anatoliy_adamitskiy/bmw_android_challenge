package com.bmw_android_challenge.utils;

import com.bmw_android_challenge.models.Location;

import java.util.List;

public interface LocationResultCallback {
    void setResultsToAdapter(List<Location> results);
}
