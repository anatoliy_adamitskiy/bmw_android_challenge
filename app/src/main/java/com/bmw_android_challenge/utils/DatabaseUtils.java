package com.bmw_android_challenge.utils;

import android.content.Context;

import java.io.File;

public class DatabaseUtils {
    public static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }
}
