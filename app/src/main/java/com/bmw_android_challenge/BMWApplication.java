package com.bmw_android_challenge;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.bmw_android_challenge.database.DatabaseHelper;

public class BMWApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        initDatabaseHelper();
    }

    private void initDatabaseHelper() {
        DatabaseHelper.initialize(this);
    }
}
