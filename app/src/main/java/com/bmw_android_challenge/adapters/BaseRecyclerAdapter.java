package com.bmw_android_challenge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.lang.ref.WeakReference;

public abstract class BaseRecyclerAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected WeakReference<Context> mContext;

    public BaseRecyclerAdapter(Context context) {
        mContext = new WeakReference<Context>(context);
    }

    protected Context getContext() {
        if( mContext == null || mContext.get() == null ) {
            Log.d("Context Error", "No context attached");
            return null;
        }

        else return mContext.get();
    }

}
