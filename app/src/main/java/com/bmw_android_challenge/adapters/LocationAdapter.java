package com.bmw_android_challenge.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bmw_android_challenge.R;
import com.bmw_android_challenge.activities.DetailActivity;
import com.bmw_android_challenge.models.Location;

import org.parceler.Parcels;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationAdapter extends BaseHeaderRecyclerAdapter<Location> {

    public static final int SORT_TYPE_NAME = 0;

    private int mCurrentSortType = SORT_TYPE_NAME;

    public LocationAdapter(Context context) {
        super(context);
    }

    // Getting header value
    @Override
    public String getHeaderValueForObject(Object object) {
        if( mCurrentSortType == SORT_TYPE_NAME ) {
            return "NAME";
        }
        return null;
    }

    @Override
    public BaseHeaderHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_location_item, parent, false);
        return new LocationViewHolder(v);
    }

    // Setting items and inserting headers
    @Override
    public void setItems(List<Location> originalItems) {
        sortItems(originalItems);
        super.setItems(originalItems);
        insertHeaders();
    }

    // Sort by method to be called when new sort is selected
    public void sortBy(int sortType ) {
        if( sortType != mCurrentSortType ) {
            mCurrentSortType = sortType;
        } else {
            return;
        }

        setItems(mOriginalItems);
        notifyDataSetChanged();
    }

    // Sorting items with comparator
    private void sortItems(List<Location> originalItems) {
        Comparator<Location> comparator = null;
        if( mCurrentSortType == SORT_TYPE_NAME ) {
            comparator = new Comparator<Location>() {
                @Override
                public int compare(Location lhs, Location rhs) {
                    return lhs.getName().compareToIgnoreCase(rhs.getName());
                }
            };

        }

        if( comparator != null && originalItems != null ) {
            Collections.sort(originalItems, comparator);
        }
    }

    @Override
    protected int getCustomItemViewType(int position) {
        return 0;
    }

    public class LocationViewHolder extends BaseHeaderHolder {

        @BindView(R.id.name) protected TextView mLocationName;
        @BindView(R.id.address) protected TextView mLocationAddress;

        public LocationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindView(int position) {
            final Location item = (Location) getItemAtPosition(position);

            if (item.getName() != null && !item.getName().isEmpty()) {
                mLocationName.setText(item.getName());
            }

            if (item.getAddress() != null && !item.getAddress().isEmpty()) {
                mLocationAddress.setVisibility(View.VISIBLE);
                mLocationAddress.setText(item.getAddress());
            } else {
                mLocationAddress.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra(getContext().getString(R.string.extra_location), Parcels.wrap(item));
                    getContext().startActivity(intent);
                }
            });
        }
    }
}
