package com.bmw_android_challenge.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bmw_android_challenge.R;
import com.bmw_android_challenge.views.HeaderRecyclerItem;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseHeaderRecyclerAdapter<V> extends BaseRecyclerAdapter<BaseHeaderRecyclerAdapter.BaseHeaderHolder> {

    public static final int HEADER_VIEW_TYPE = -10;
    protected List<V> mOriginalItems;
    protected List<HeaderRecyclerItem> mItems = new ArrayList<>();

    public BaseHeaderRecyclerAdapter(Context context) {
        super(context);
        mOriginalItems = new ArrayList<>();
    }

    // Setting items to adapter
    public void setItems(List<V> originalItems) {
        this.mOriginalItems = originalItems;
        mItems.clear();
        mItems.addAll(wrapOriginalItems());
    }

    public Object getItemAtPosition(int position) {
        return mItems.get(position).getObject();
    }

    // Getting item view type
    @Override
    public int getItemViewType(int position) {
        if ( mItems != null && !mItems.isEmpty() && mItems.get(position).isHeader()) {
            return HEADER_VIEW_TYPE;
        }
        return getCustomItemViewType(position);
    }

    @Override
    public BaseHeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType != HEADER_VIEW_TYPE) {
            return onCreateItemViewHolder(parent, viewType);
        } else {
            return new BaseHeaderViewHolder(
                    LayoutInflater.from(getContext())
                            .inflate(R.layout.view_holder_header, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(BaseHeaderHolder holder, int position) {
        holder.bindView(position);
    }

    // Insertion of headers on setItems
    public void insertHeaders() {
        int headerPos;
        List<Pair<Integer, HeaderRecyclerItem>> headers = new ArrayList<>();
        HeaderRecyclerItem header;
        for (HeaderRecyclerItem item : mItems) {
            if (!item.isHeader()) {
                String headerText = getHeaderValueForObject(item.getObject());
                if (!headerExists(headerText, headers)) {
                    headerPos = mItems.indexOf(item);
                    header = wrapObject(headerText, true);
                    headers.add(Pair.create(headerPos + headers.size(), header));
                }
            }
        }

        for (Pair<Integer, HeaderRecyclerItem> newHeader : headers) {
            mItems.add(newHeader.first, newHeader.second);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public abstract String getHeaderValueForObject(Object object);

    private boolean headerExists(String focusToCheck,
                                 List<Pair<Integer, HeaderRecyclerItem>> headers) {
        for (Pair<Integer, HeaderRecyclerItem> header : headers) {
            if (header.second.getObject().equals(focusToCheck)) {
                return true;
            }
        }
        return false;
    }

    // Wrapping headers and objects
    private List<HeaderRecyclerItem> wrapOriginalItems() {
        List<HeaderRecyclerItem> wrappedList = new ArrayList<>();

        if( mOriginalItems != null ) {
            for (V originalType : mOriginalItems) {
                wrappedList.add(wrapObject(originalType, false));
            }
        }
        return wrappedList;
    }

    @NonNull
    private HeaderRecyclerItem wrapObject(Object object, boolean isHeader) {
        return new HeaderRecyclerItem(object, isHeader);
    }

    public class BaseHeaderViewHolder extends BaseHeaderHolder {

        TextView headerText;

        public BaseHeaderViewHolder(View itemView) {
            super(itemView);
            headerText = (TextView) itemView.findViewById(R.id.textView_header);
        }

        @Override
        public void bindView(int position) {
            headerText.setText((String) mItems.get(position).getObject());
        }
    }

    public abstract BaseHeaderHolder onCreateItemViewHolder(ViewGroup parent, int viewType);
    protected abstract int getCustomItemViewType(int position);

    public static abstract class BaseHeaderHolder extends RecyclerView.ViewHolder {

        public BaseHeaderHolder(View itemView) {
            super(itemView);
        }

        public abstract void bindView(int position);
    }

}
