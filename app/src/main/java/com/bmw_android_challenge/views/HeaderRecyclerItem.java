package com.bmw_android_challenge.views;

public class HeaderRecyclerItem {

    private final Object object;
    private final boolean isHeader;

    public HeaderRecyclerItem(Object object, boolean isHeader) {
        this.object = object;
        this.isHeader = isHeader;
    }

    public Object getObject() {
        return object;
    }

    public boolean isHeader() {
        return isHeader;
    }
}
